#include <iostream>
#include "header.h"

using namespace std;

void floatList::appendNode(float number) { // appending a node onto the end of the current list.
    ListNode* new_Node = new ListNode; // this creates a new node called "new_Node"
    new_Node->value = number; // this sets the node in question to the number passed to it by the main function.
    new_Node->next = nullptr; // this sets the next pointer to "nullptr"

    if (head == nullptr) { // first i need to check if the list is empty or not. which means the head pointer is empty or "nullptr"
        head = new_Node; // in this case it sets the head pointer to the node in question.
    }
    else {
        ListNode* current_Node = head; // if the head pointer has a value, it sets a new node variable to "current_Node"
        while (current_Node->next != nullptr) { // this flips through each node in the list checking if it is not equal to a nullptr
            current_Node = current_Node->next;
        }
        current_Node->next = new_Node; // once it finds a nullptr it sets the current node in question to the new node passed by the main function.
    }
}

void floatList::displayList() { // this is to print each node in the list.
    ListNode* current_Node = head; // this is to set the current node to the first node in the list
    while (current_Node != nullptr) { // this flips through each node in the list outputting each one.
        cout << current_Node->value << endl;
        current_Node = current_Node->next;
    }
}


void floatList::deleteNode(float number) { // this is to find a particlaur node and remove it from the list of nodes.
    if (head->value == number) { // firstly i check if the head pointer is the number that i need to delete
        ListNode* temp = head; // I set a temporary node to the head pointer.
        head = head->next; // I make the next item the head pointer
        delete temp; // I discard the temp variable.
    }
    else { // if the head pointer is not the number that i need to delete then it flips through each item in the list.
        // Traverse the list to find the node before the one to delete
        ListNode* current_Node = head; // sets the current node to the head pointer
        while (current_Node->next != nullptr && current_Node->next->value != number) {
        // this flips through each node in teh list checking that the next node isnt a null pointer and the next node's value isnt the number passed by the main function.
            current_Node = current_Node->next;
        }

        if (current_Node->next == nullptr) { // if the value given by the main functino couldnt be found then nothing happens.
            return;
        }

        ListNode* temp = current_Node->next; // once the node to be deleted has been found, a temporary file is set to the delted node
        current_Node->next = current_Node->next->next; // the next node is set to the old node's position
        delete temp; // the old node is discarded
    }
    cout << number << " is deleted" << endl; // lastly, print the deleted message after the number that was deleted.
}