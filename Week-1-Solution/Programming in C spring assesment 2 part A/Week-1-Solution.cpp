#include<iostream>

using namespace std;

int matrix_1[5][5];
int matrix_2[5][5];
int matrix_3[5][5];
int a;
int b;
int numrows;
int numcols;

int main()
{
	cout << "Please enter the number of rows (max 5) = ";
	cin >> numrows;
	cout << "Please enter the number of column (max 5) = ";
	cin >> numcols;

	cout << "1st Matrix Input:";
	for (a = 0;a < numrows;a++)
	{
		for (b = 0;b < numcols;b++)
		{
			cout << "\nmatrix1[" << a << "][" << b << "]=  ";
			cin >> matrix_1[a][b];
		}
	}

	cout << "2nd Matrix Input:";
	for (a = 0;a < numrows;a++)
	{
		for (b = 0;b < numcols;b++)
		{
			cout << "\nmatrix2[" << a << "][" << b << "]=  ";
			cin >> matrix_2[a][b];
		}
	}

	cout << "Adding Matrices...";

	for (a = 0;a < numrows;a++)
	{
		for (b = 0;b < numcols;b++)
		{
			matrix_3[a][b] = matrix_1[a][b] + matrix_2[a][b];
		}
	}

	cout << "\nThe resultant Matrix is:\n";

	for (a = 0;a < numrows;a++)
	{
		for (b = 0;b < numcols;b++)
		{
			cout << "\t" << matrix_3[a][b];
		}
		cout << endl;
	}

}